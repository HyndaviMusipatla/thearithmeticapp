//
//  ViewController.swift
//  The ArithMETic App
//
//  Created by student on 2/14/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
   
    
    
    @IBOutlet weak var viewpicker: UIPickerView!
    
    @IBOutlet weak var weightTF: UITextField!
    
    @IBOutlet weak var exerciseTF: UITextField!
    
    @IBOutlet weak var timeTF: UILabel!
    
    @IBOutlet weak var energyTF: UILabel!
    
    var typePicker: [String] = [String] ()
    
    
    @IBAction func clearBT(_ sender: Any) {
        weightTF.text! = ""
        exerciseTF.text! = ""
        energyTF.text! = "0 cal"
        timeTF.text! = "0 minutes"
        viewpicker.selectRow(0, inComponent: 0, animated: true)
        
    }
    
    @IBAction func calculateBT(_ sender: Any) {
        let value = typePicker[viewpicker.selectedRow(inComponent: 0)]
        let defaultValue = 0.0
        let energyConsumed = self.energyConsumed(during: value, weight: Double(weightTF.text!) ?? defaultValue, time: Double(exerciseTF.text!) ?? defaultValue)
        energyTF.text = String(format: "%.1f cal", energyConsumed)
        let timeReqToLose = timeToLose1pound(during: value, weight: Double(weightTF.text!) ?? defaultValue)
        timeTF.text! = String(format: "%.1f minutes", timeReqToLose)
        
    
    
    }
    
    
    
    func energyConsumed(during: String,weight: Double, time: Double) -> Double {
        var activity = 0.0
        switch during {
        case "Bicycling", "Tennis":
            activity = 8.0
        case "Jumping rope":
            activity = 12.3
        case "Running - slow":
            activity = 9.8
        case "Running - fast":
            activity = 23.0
        case "Swimming":
            activity = 5.8
            
        default:
            activity = 0.0
        }
        let weightInkg = Double(weight)/2.2
        return activity * 3.5 * weightInkg/Double(200) * time
        
    }
    func timeToLose1pound(during: String,weight: Double) -> Double {
        let energyInCalories = energyConsumed(during: during, weight: weight, time: 1.0)
        return Double(3500)/energyInCalories
        
       
    }
    
   override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = #colorLiteral(red: 0.9665284201, green: 0.6460195391, blue: 1, alpha: 1)
        
        self.viewpicker.delegate = self
        
        self.viewpicker.dataSource = self
        
        typePicker = ["Bicycling", "Jumping rope", "Running - slow", "Running - fast", "Tennis", "Swimming" ]
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return typePicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return typePicker[row]
    }
    



}

